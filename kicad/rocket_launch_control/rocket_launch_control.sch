EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 1 2
Title "Rocket Launch Controller"
Date "2021-05-21"
Rev ""
Comp "Stuart Patterson"
Comment1 "Creative Commons Attribution-ShareAlike 4.0 International License"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Transistor_FET:IRF540N Q1
U 1 1 605A3FFC
P 6750 2250
F 0 "Q1" H 6954 2296 50  0000 L CNN
F 1 "IRF540N" H 6950 2150 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7000 2175 50  0001 L CIN
F 3 "http://www.irf.com/product-info/datasheets/data/irf540n.pdf" H 6750 2250 50  0001 L CNN
	1    6750 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 605B4D5F
P 5450 4650
F 0 "#PWR018" H 5450 4400 50  0001 C CNN
F 1 "GND" H 5455 4477 50  0000 C CNN
F 2 "" H 5450 4650 50  0001 C CNN
F 3 "" H 5450 4650 50  0001 C CNN
	1    5450 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R17
U 1 1 605B7345
P 6400 2400
F 0 "R17" H 6330 2354 50  0000 R CNN
F 1 "10K" H 6330 2445 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6330 2400 50  0001 C CNN
F 3 "~" H 6400 2400 50  0001 C CNN
	1    6400 2400
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR025
U 1 1 605BECEB
P 7850 2500
F 0 "#PWR025" H 7850 2250 50  0001 C CNN
F 1 "GND" H 7855 2327 50  0000 C CNN
F 2 "" H 7850 2500 50  0001 C CNN
F 3 "" H 7850 2500 50  0001 C CNN
	1    7850 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR022
U 1 1 605D3745
P 6850 2450
F 0 "#PWR022" H 6850 2200 50  0001 C CNN
F 1 "GND" H 6855 2277 50  0000 C CNN
F 2 "" H 6850 2450 50  0001 C CNN
F 3 "" H 6850 2450 50  0001 C CNN
	1    6850 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR021
U 1 1 605D429E
P 6400 2550
F 0 "#PWR021" H 6400 2300 50  0001 C CNN
F 1 "GND" H 6405 2377 50  0000 C CNN
F 2 "" H 6400 2550 50  0001 C CNN
F 3 "" H 6400 2550 50  0001 C CNN
	1    6400 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 2050 7250 2300
Wire Wire Line
	6550 2250 6400 2250
Wire Wire Line
	6200 2250 6400 2250
Connection ~ 6400 2250
Text Label 6850 1450 0    50   ~ 0
14.8v
Text Label 5050 3400 2    50   ~ 0
D4_READY3_CLR_BTN
Text Label 7350 1450 0    50   ~ 0
14.8v
Text Label 5750 2600 0    50   ~ 0
5v
NoConn ~ 5650 2600
NoConn ~ 6050 3000
NoConn ~ 6050 3100
NoConn ~ 6050 3400
Text Label 700  3350 2    50   ~ 0
5v
$Comp
L Transistor_FET:IRF540N Q2
U 1 1 605F6A25
P 7300 6100
F 0 "Q2" H 7504 6146 50  0000 L CNN
F 1 "IRF540N" H 7500 6000 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 7550 6025 50  0001 L CIN
F 3 "http://www.irf.com/product-info/datasheets/data/irf540n.pdf" H 7300 6100 50  0001 L CNN
	1    7300 6100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R18
U 1 1 605F6A2B
P 6950 6250
F 0 "R18" H 6880 6204 50  0000 R CNN
F 1 "10K" H 6880 6295 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 6880 6250 50  0001 C CNN
F 3 "~" H 6950 6250 50  0001 C CNN
	1    6950 6250
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR026
U 1 1 605F6A37
P 8400 6350
F 0 "#PWR026" H 8400 6100 50  0001 C CNN
F 1 "GND" H 8405 6177 50  0000 C CNN
F 2 "" H 8400 6350 50  0001 C CNN
F 3 "" H 8400 6350 50  0001 C CNN
	1    8400 6350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR024
U 1 1 605F6A3D
P 7400 6300
F 0 "#PWR024" H 7400 6050 50  0001 C CNN
F 1 "GND" H 7405 6127 50  0000 C CNN
F 2 "" H 7400 6300 50  0001 C CNN
F 3 "" H 7400 6300 50  0001 C CNN
	1    7400 6300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR023
U 1 1 605F6A43
P 6950 6400
F 0 "#PWR023" H 6950 6150 50  0001 C CNN
F 1 "GND" H 6955 6227 50  0000 C CNN
F 2 "" H 6950 6400 50  0001 C CNN
F 3 "" H 6950 6400 50  0001 C CNN
	1    6950 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 5900 7800 6150
Wire Wire Line
	7100 6100 6950 6100
Wire Wire Line
	6750 6100 6950 6100
Text Label 7400 5300 0    50   ~ 0
14.8v
Text Label 7500 5150 2    50   ~ 0
A6_CONTINUITY_ROCKET2_TEST
Text Label 7900 5300 0    50   ~ 0
14.8v
Text Label 5050 3600 2    50   ~ 0
D6_READY2_CLR_LED
Text Label 700  4700 2    50   ~ 0
5v
Text Label 2150 3900 2    50   ~ 0
5v
Text Label 5050 3700 2    50   ~ 0
D7_READY1_CLR_LED
Text Label 5050 3800 2    50   ~ 0
D8_ROCKET1_SWITCH
Text Label 5050 3900 2    50   ~ 0
D9_ROCKET2_SWITCH
$Comp
L Device:R R8
U 1 1 60613F55
P 3550 1000
F 0 "R8" V 3757 1000 50  0000 C CNN
F 1 "330" V 3666 1000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3480 1000 50  0001 C CNN
F 3 "~" H 3550 1000 50  0001 C CNN
	1    3550 1000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R15
U 1 1 60618F56
P 5200 1000
F 0 "R15" V 5407 1000 50  0000 C CNN
F 1 "330" V 5316 1000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5130 1000 50  0001 C CNN
F 3 "~" H 5200 1000 50  0001 C CNN
	1    5200 1000
	0    -1   -1   0   
$EndComp
$Comp
L rocket_launch_control-rescue:Arduino_Nano_v3.x-MCU_Module A1
U 1 1 60593920
P 5550 3600
F 0 "A1" H 6150 2550 50  0000 C CNN
F 1 "Arduino_Nano_v3.x" H 6150 2650 50  0000 C CNN
F 2 "Module:Arduino_Nano" H 5550 3600 50  0001 C CIN
F 3 "http://www.mouser.com/pdfdocs/Gravitech_Arduino_Nano3_0.pdf" H 5550 3600 50  0001 C CNN
	1    5550 3600
	1    0    0    -1  
$EndComp
Text Label 5050 4200 2    50   ~ 0
D12_LAUNCH_ROCKET1
Text Label 5050 3500 2    50   ~ 0
D5_LAUNCH_ROCKET2
$Comp
L power:GND #PWR020
U 1 1 60622922
P 5750 4650
F 0 "#PWR020" H 5750 4400 50  0001 C CNN
F 1 "GND" H 5755 4477 50  0000 C CNN
F 2 "" H 5750 4650 50  0001 C CNN
F 3 "" H 5750 4650 50  0001 C CNN
	1    5750 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 4650 5450 4600
Wire Wire Line
	5450 4600 5550 4600
Wire Wire Line
	5650 4600 5750 4600
Wire Wire Line
	5750 4600 5750 4650
$Comp
L Device:R R19
U 1 1 605ACFC5
P 7150 850
F 0 "R19" V 6943 850 50  0000 C CNN
F 1 "330" V 7034 850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7080 850 50  0001 C CNN
F 3 "~" H 7150 850 50  0001 C CNN
	1    7150 850 
	-1   0    0    1   
$EndComp
$Comp
L Device:R R20
U 1 1 605AE3EA
P 7700 4700
F 0 "R20" V 7493 4700 50  0000 C CNN
F 1 "330" V 7584 4700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 7630 4700 50  0001 C CNN
F 3 "~" H 7700 4700 50  0001 C CNN
	1    7700 4700
	-1   0    0    1   
$EndComp
Text Label 6050 3900 0    50   ~ 0
A3_LAUNCH_SAFETY_BTN_LED
Text Label 6050 4100 0    50   ~ 0
A5_CONTINUITY_OUTPUT_5V
$Comp
L Connector:Conn_Coaxial J18
U 1 1 605A69F9
P 8400 6150
F 0 "J18" H 8500 6125 50  0000 L CNN
F 1 "RCA_ROCKET_2" H 8500 6034 50  0000 L CNN
F 2 "jd1914_auto_relay:RCA_PADS" H 8400 6150 50  0001 C CNN
F 3 " ~" H 8400 6150 50  0001 C CNN
	1    8400 6150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J16
U 1 1 605A85E2
P 7850 2300
F 0 "J16" H 7950 2275 50  0000 L CNN
F 1 "RCA_ROCKET_1" H 7950 2150 50  0000 L CNN
F 2 "jd1914_auto_relay:RCA_PADS" H 7850 2300 50  0001 C CNN
F 3 " ~" H 7850 2300 50  0001 C CNN
	1    7850 2300
	1    0    0    -1  
$EndComp
$Comp
L jd1914_auto_relay:JD1914 K1
U 1 1 605BFA0E
P 7050 1750
F 0 "K1" H 7480 1796 50  0000 L CNN
F 1 "JD1914" H 7480 1705 50  0000 L CNN
F 2 "jd1914_auto_relay:jd1914 auto relay" H 7500 1700 50  0001 L CNN
F 3 "" H 7050 1750 50  0001 C CNN
	1    7050 1750
	1    0    0    -1  
$EndComp
$Comp
L jd1914_auto_relay:JD1914 K2
U 1 1 605C1134
P 7600 5600
F 0 "K2" H 8030 5646 50  0000 L CNN
F 1 "JD1914" H 8030 5555 50  0000 L CNN
F 2 "jd1914_auto_relay:jd1914 auto relay" H 8050 5550 50  0001 L CNN
F 3 "" H 7600 5600 50  0001 C CNN
	1    7600 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 1450 7150 1300
Wire Wire Line
	7700 5150 7700 5300
Text Notes 5300 2650 2    50   ~ 0
vin support\n7-12v input
NoConn ~ 5450 2600
Text Label 5050 4300 2    50   ~ 0
D13_READY3_CLR_LED
NoConn ~ 5050 3000
NoConn ~ 5050 3100
Text Label 6050 3600 0    50   ~ 0
A0_LAUNCH_BTN
Text Notes 6950 3000 0    50   ~ 0
JD1914 Relay Colors\n30 - Black\n85 - White\n86 - Yellow\n87 - Red\n87a - Blue 
Text Notes 6000 1400 0    50   ~ 0
LOW = Continuity
Text Notes 6650 5250 0    50   ~ 0
LOW = Continuity
$Comp
L Amplifier_Audio:LM386 U1
U 1 1 6066D40E
P 4950 5700
F 0 "U1" H 5100 5950 50  0000 L CNN
F 1 "LM386" H 5050 5850 50  0000 L CNN
F 2 "Package_DIP:DIP-8_W7.62mm_Socket" H 5050 5800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm386.pdf" H 5150 5900 50  0001 C CNN
	1    4950 5700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C4
U 1 1 60670FAF
P 5600 5700
F 0 "C4" V 5855 5700 50  0000 C CNN
F 1 "220uF" V 5764 5700 50  0000 C CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 5638 5550 50  0001 C CNN
F 3 "~" H 5600 5700 50  0001 C CNN
	1    5600 5700
	0    -1   -1   0   
$EndComp
Text Label 4850 5400 1    50   ~ 0
5v
$Comp
L power:GND #PWR03
U 1 1 60672B1F
P 4850 6000
F 0 "#PWR03" H 4850 5750 50  0001 C CNN
F 1 "GND" H 4855 5827 50  0000 C CNN
F 2 "" H 4850 6000 50  0001 C CNN
F 3 "" H 4850 6000 50  0001 C CNN
	1    4850 6000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 60676D1B
P 5300 6450
F 0 "#PWR08" H 5300 6200 50  0001 C CNN
F 1 "GND" H 5305 6277 50  0000 C CNN
F 2 "" H 5300 6450 50  0001 C CNN
F 3 "" H 5300 6450 50  0001 C CNN
	1    5300 6450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 6067774D
P 5300 6000
F 0 "C3" H 5415 6046 50  0000 L CNN
F 1 "56nF" H 5415 5955 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 5338 5850 50  0001 C CNN
F 3 "~" H 5300 6000 50  0001 C CNN
	1    5300 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 5700 5300 5700
Wire Wire Line
	5300 5850 5300 5700
Connection ~ 5300 5700
Wire Wire Line
	5300 5700 5450 5700
$Comp
L power:GND #PWR01
U 1 1 606812E2
P 4600 6000
F 0 "#PWR01" H 4600 5750 50  0001 C CNN
F 1 "GND" H 4605 5827 50  0000 C CNN
F 2 "" H 4600 6000 50  0001 C CNN
F 3 "" H 4600 6000 50  0001 C CNN
	1    4600 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 6000 4600 5800
Wire Wire Line
	4600 5800 4650 5800
NoConn ~ 4950 5400
NoConn ~ 5050 6000
NoConn ~ 4950 6000
$Comp
L Device:R R5
U 1 1 60663170
P 5300 6300
F 0 "R5" H 5370 6346 50  0000 L CNN
F 1 "10" H 5370 6255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5230 6300 50  0001 C CNN
F 3 "~" H 5300 6300 50  0001 C CNN
	1    5300 6300
	1    0    0    -1  
$EndComp
Text Label 6050 3800 0    50   ~ 0
A2_LAUNCH_BTN_LED
Wire Wire Line
	6900 1300 7150 1300
Wire Wire Line
	7500 5150 7700 5150
Text Label 5050 4000 2    50   ~ 0
D10_RX
Text Label 5050 4100 2    50   ~ 0
D11_TX
Text Label 5050 3200 2    50   ~ 0
D2_READY1_CLR_BTN
Text Label 5050 3300 2    50   ~ 0
D3_READY2_CLR_BTN
Text Label 3800 1000 0    50   ~ 0
D8_ROCKET1_SWITCH
Text Label 6200 2250 2    50   ~ 0
D12_LAUNCH_ROCKET1
Text Label 9750 1500 2    50   ~ 0
5v
Text Label 6050 3700 0    50   ~ 0
A1_LAUNCH_SAFETY_BTN
Text Label 6050 4000 0    50   ~ 0
A4_CONTINUITY_ROCKET1_TEST
Text Label 6900 1300 2    50   ~ 0
A4_CONTINUITY_ROCKET1_TEST
Text Notes 5950 4900 0    50   ~ 0
A6 and A7 can \nonly used as inputs
Text Label 6050 4200 0    50   ~ 0
A6_CONTINUITY_ROCKET2_TEST
Text Label 7150 700  2    50   ~ 0
A5_CONTINUITY_OUTPUT_5V
Text Label 7700 4550 2    50   ~ 0
A5_CONTINUITY_OUTPUT_5V
Text Label 6050 4300 0    50   ~ 0
A7_DFPLAYER_BUSY
Text Label 3100 1000 2    50   ~ 0
5v
Text Label 4750 1000 2    50   ~ 0
5v
$Comp
L Device:R R9
U 1 1 6071298E
P 3750 1250
F 0 "R9" V 3957 1250 50  0000 C CNN
F 1 "10K" V 3866 1250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3680 1250 50  0001 C CNN
F 3 "~" H 3750 1250 50  0001 C CNN
	1    3750 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R16
U 1 1 60712E05
P 5350 1200
F 0 "R16" V 5557 1200 50  0000 C CNN
F 1 "10K" V 5466 1200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5280 1200 50  0001 C CNN
F 3 "~" H 5350 1200 50  0001 C CNN
	1    5350 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 1000 5350 1050
Wire Wire Line
	5350 1000 5400 1000
Connection ~ 5350 1000
$Comp
L power:GND #PWR017
U 1 1 6073381C
P 5350 1350
F 0 "#PWR017" H 5350 1100 50  0001 C CNN
F 1 "GND" H 5355 1177 50  0000 C CNN
F 2 "" H 5350 1350 50  0001 C CNN
F 3 "" H 5350 1350 50  0001 C CNN
	1    5350 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR012
U 1 1 60733CD7
P 3750 1400
F 0 "#PWR012" H 3750 1150 50  0001 C CNN
F 1 "GND" H 3755 1227 50  0000 C CNN
F 2 "" H 3750 1400 50  0001 C CNN
F 3 "" H 3750 1400 50  0001 C CNN
	1    3750 1400
	1    0    0    -1  
$EndComp
Text Notes 900  2700 0    50   ~ 0
4 x18650= 14.8v\nTypical output 2-3.0A
$Comp
L power:GND #PWR09
U 1 1 6067330F
P 5700 6100
F 0 "#PWR09" H 5700 5850 50  0001 C CNN
F 1 "GND" H 5705 5927 50  0000 C CNN
F 2 "" H 5700 6100 50  0001 C CNN
F 3 "" H 5700 6100 50  0001 C CNN
	1    5700 6100
	1    0    0    -1  
$EndComp
Text Label 9350 2250 0    50   ~ 0
A3_LAUNCH_SAFETY_BTN_LED
$Comp
L power:GND #PWR031
U 1 1 606FF4BE
P 9700 1950
F 0 "#PWR031" H 9700 1700 50  0001 C CNN
F 1 "GND" H 9705 1777 50  0000 C CNN
F 2 "" H 9700 1950 50  0001 C CNN
F 3 "" H 9700 1950 50  0001 C CNN
	1    9700 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R25
U 1 1 606FF4B8
P 9350 1950
F 0 "R25" V 9143 1950 50  0000 C CNN
F 1 "330" V 9234 1950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9280 1950 50  0001 C CNN
F 3 "~" H 9350 1950 50  0001 C CNN
	1    9350 1950
	-1   0    0    1   
$EndComp
Connection ~ 9000 1600
Wire Wire Line
	9000 1750 9000 1600
$Comp
L power:GND #PWR030
U 1 1 606FF4B0
P 9000 2050
F 0 "#PWR030" H 9000 1800 50  0001 C CNN
F 1 "GND" H 9005 1877 50  0000 C CNN
F 2 "" H 9000 2050 50  0001 C CNN
F 3 "" H 9000 2050 50  0001 C CNN
	1    9000 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R22
U 1 1 606FF4AA
P 9000 1900
F 0 "R22" V 8793 1900 50  0000 C CNN
F 1 "10K" V 8884 1900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8930 1900 50  0001 C CNN
F 3 "~" H 9000 1900 50  0001 C CNN
	1    9000 1900
	1    0    0    -1  
$EndComp
Text Label 9000 1600 2    50   ~ 0
A1_LAUNCH_SAFETY_BTN
Wire Wire Line
	9000 1300 9000 1200
Wire Wire Line
	9100 1600 9000 1600
$Comp
L power:GND #PWR029
U 1 1 606FF49F
P 9000 1200
F 0 "#PWR029" H 9000 950 50  0001 C CNN
F 1 "GND" H 9005 1027 50  0000 C CNN
F 2 "" H 9000 1200 50  0001 C CNN
F 3 "" H 9000 1200 50  0001 C CNN
	1    9000 1200
	-1   0    0    1   
$EndComp
$Comp
L Device:C C7
U 1 1 606FF499
P 9000 1450
F 0 "C7" H 9115 1496 50  0000 L CNN
F 1 "0.1uF" H 9115 1405 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 9038 1300 50  0001 C CNN
F 3 "~" H 9000 1450 50  0001 C CNN
	1    9000 1450
	-1   0    0    1   
$EndComp
$Comp
L Device:R R24
U 1 1 606FF493
P 9250 1600
F 0 "R24" V 9043 1600 50  0000 C CNN
F 1 "330" V 9134 1600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9180 1600 50  0001 C CNN
F 3 "~" H 9250 1600 50  0001 C CNN
	1    9250 1600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3200 1000 3400 1000
Wire Wire Line
	3700 1000 3750 1000
Wire Wire Line
	3750 1100 3750 1000
Connection ~ 3750 1000
Wire Wire Line
	3750 1000 3800 1000
Wire Wire Line
	4850 1000 5050 1000
Wire Wire Line
	7700 5150 7700 5050
Connection ~ 7700 5150
Wire Wire Line
	7700 4950 7700 4850
Wire Wire Line
	7150 1100 7150 1000
Wire Wire Line
	7150 1200 7150 1300
Connection ~ 7150 1300
$Comp
L Connector:Conn_01x04_Male J19
U 1 1 60A713E6
P 9950 1700
F 0 "J19" H 9922 1582 50  0000 R CNN
F 1 "SAFETY_BTN_LED" H 10500 1400 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B4B-XH-A_1x04_P2.50mm_Vertical" H 9950 1700 50  0001 C CNN
F 3 "~" H 9950 1700 50  0001 C CNN
	1    9950 1700
	-1   0    0    1   
$EndComp
Wire Wire Line
	9400 1600 9750 1600
Wire Wire Line
	9750 1800 9700 1800
Wire Wire Line
	9700 1800 9700 1950
Wire Wire Line
	9350 1800 9350 1700
Wire Wire Line
	9350 1700 9750 1700
Wire Wire Line
	9350 2250 9350 2100
$Comp
L Connector:Conn_01x02_Male J15
U 1 1 60AD03F8
P 7350 1200
F 0 "J15" H 7322 1082 50  0000 R CNN
F 1 "CONTINUITY_ROCKET1_LED" H 7322 1173 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B2B-XH-A_1x02_P2.50mm_Vertical" H 7350 1200 50  0001 C CNN
F 3 "~" H 7350 1200 50  0001 C CNN
	1    7350 1200
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J17
U 1 1 60AE023C
P 7900 5050
F 0 "J17" H 7872 4932 50  0000 R CNN
F 1 "CONTINUITY_ROCKET2_LED" H 7872 5023 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B2B-XH-A_1x02_P2.50mm_Vertical" H 7900 5050 50  0001 C CNN
F 3 "~" H 7900 5050 50  0001 C CNN
	1    7900 5050
	-1   0    0    1   
$EndComp
$Comp
L digital-audio:DFPlayer_Mini U2
U 1 1 605B9C78
P 4900 7400
F 0 "U2" H 4875 7925 50  0000 C CNN
F 1 "DFPlayer_Mini" H 4875 7834 50  0000 C CNN
F 2 "Arduino_Nano:DFPlayer_V2" H 4800 7350 50  0001 C CNN
F 3 "" H 4800 7350 50  0000 C CNN
	1    4900 7400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 605BFF1B
P 5350 7700
F 0 "#PWR019" H 5350 7450 50  0001 C CNN
F 1 "GND" H 5355 7527 50  0000 C CNN
F 2 "" H 5350 7700 50  0001 C CNN
F 3 "" H 5350 7700 50  0001 C CNN
	1    5350 7700
	0    -1   -1   0   
$EndComp
Text Label 4400 7100 2    50   ~ 0
5v
NoConn ~ 4400 7700
NoConn ~ 5350 7200
NoConn ~ 5350 7300
NoConn ~ 5350 7400
NoConn ~ 5350 7500
$Comp
L Device:R R14
U 1 1 605DB8E4
P 4150 7150
F 0 "R14" V 3943 7150 50  0000 C CNN
F 1 "1K" V 4034 7150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4080 7150 50  0001 C CNN
F 3 "~" H 4150 7150 50  0001 C CNN
	1    4150 7150
	0    1    1    0   
$EndComp
NoConn ~ 4400 7800
NoConn ~ 4400 7600
NoConn ~ 5350 7800
NoConn ~ 5350 7600
Text Label 4300 7400 2    50   ~ 0
AUDIO_OUT_DAC_R
Text Label 5350 7100 0    50   ~ 0
A7_DFPLAYER_BUSY
Wire Wire Line
	4400 7400 4300 7400
NoConn ~ 4400 7500
Text Label 4400 7300 2    50   ~ 0
D10_RX
Text Label 6750 6100 2    50   ~ 0
D5_LAUNCH_ROCKET2
Text Label 4000 7150 2    50   ~ 0
D11_TX
Text Label 4650 5500 1    50   ~ 0
AUDIO_OUT_DAC_R
Wire Wire Line
	5700 6100 5700 5800
Wire Wire Line
	5700 5800 5750 5800
$Comp
L Connector:Conn_01x03_Male J3
U 1 1 60B5D9B8
P 4450 5600
F 0 "J3" H 4450 5400 50  0000 C CNN
F 1 "VOLUME" V 4350 5600 50  0000 C CNN
F 2 "Connector_JST:JST_XH_B3B-XH-A_1x03_P2.50mm_Vertical" H 4450 5600 50  0001 C CNN
F 3 "~" H 4450 5600 50  0001 C CNN
	1    4450 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 5700 4650 5800
Connection ~ 4650 5800
Wire Wire Line
	4300 7150 4300 7200
Wire Wire Line
	4300 7200 4400 7200
$Comp
L Connector:Conn_01x02_Male J8
U 1 1 60C6982C
P 3100 1200
F 0 "J8" H 3072 1082 50  0000 R CNN
F 1 "ROCKET1_SWITCH" V 2950 1600 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B2B-XH-A_1x02_P2.50mm_Vertical" H 3100 1200 50  0001 C CNN
F 3 "~" H 3100 1200 50  0001 C CNN
	1    3100 1200
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x02_Male J14
U 1 1 60C868C7
P 4750 1200
F 0 "J14" H 4722 1082 50  0000 R CNN
F 1 "ROCKET2_SWITCH" V 4600 1600 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B2B-XH-A_1x02_P2.50mm_Vertical" H 4750 1200 50  0001 C CNN
F 3 "~" H 4750 1200 50  0001 C CNN
	1    4750 1200
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x02_Male J6
U 1 1 60E3AA16
P 5950 5800
F 0 "J6" H 5922 5682 50  0000 R CNN
F 1 "SPEAKER" H 6150 5600 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B2B-XH-A_1x02_P2.50mm_Vertical" H 5950 5800 50  0001 C CNN
F 3 "~" H 5950 5800 50  0001 C CNN
	1    5950 5800
	-1   0    0    1   
$EndComp
Text Label 5400 1000 0    50   ~ 0
D9_ROCKET2_SWITCH
Text Notes 8550 6500 0    50   ~ 0
4 x18650= 14.8v\nTypical output 2-3.0A
$Sheet
S 800  650  950  250 
U 6105AEEC
F0 "Cable Connected Parks" 50
F1 "cable_connected_parts.sch" 50
$EndSheet
Text Notes 2000 2600 0    50   ~ 0
Reverse Voltage \nProtection
$Comp
L Connector:Conn_01x02_Male J13
U 1 1 6112B29A
P 4300 2650
AR Path="/6112B29A" Ref="J13"  Part="1" 
AR Path="/6105AEEC/6112B29A" Ref="J?"  Part="1" 
F 0 "J13" H 4272 2532 50  0000 R CNN
F 1 "POWER_LED_M" H 4600 2450 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B2B-XH-A_1x02_P2.50mm_Vertical" H 4300 2650 50  0001 C CNN
F 3 "~" H 4300 2650 50  0001 C CNN
	1    4300 2650
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R13
U 1 1 6112B2A0
P 4150 2300
AR Path="/6112B2A0" Ref="R13"  Part="1" 
AR Path="/6105AEEC/6112B2A0" Ref="R?"  Part="1" 
F 0 "R13" V 4250 2400 50  0000 R CNN
F 1 "330" V 4150 2350 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 4080 2300 50  0001 C CNN
F 3 "~" H 4150 2300 50  0001 C CNN
	1    4150 2300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR016
U 1 1 61151F40
P 4400 2450
F 0 "#PWR016" H 4400 2200 50  0001 C CNN
F 1 "GND" H 4405 2277 50  0000 C CNN
F 2 "" H 4400 2450 50  0001 C CNN
F 3 "" H 4400 2450 50  0001 C CNN
	1    4400 2450
	-1   0    0    1   
$EndComp
Text Label 3900 2150 0    50   ~ 0
5v
$Comp
L Connector:Conn_01x02_Male J10
U 1 1 608C15AF
P 3850 5200
F 0 "J10" H 3822 5082 50  0000 R CNN
F 1 "READY1_LED" V 3800 5950 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B2B-XH-A_1x02_P2.50mm_Vertical" H 3850 5200 50  0001 C CNN
F 3 "~" H 3850 5200 50  0001 C CNN
	1    3850 5200
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR015
U 1 1 609637E3
P 3950 6200
F 0 "#PWR015" H 3950 5950 50  0001 C CNN
F 1 "GND" H 3955 6027 50  0000 C CNN
F 2 "" H 3950 6200 50  0001 C CNN
F 3 "" H 3950 6200 50  0001 C CNN
	1    3950 6200
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x02_Male J12
U 1 1 6093968A
P 3850 6400
F 0 "J12" H 3822 6282 50  0000 R CNN
F 1 "READY3_LED" V 3800 7150 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B2B-XH-A_1x02_P2.50mm_Vertical" H 3850 6400 50  0001 C CNN
F 3 "~" H 3850 6400 50  0001 C CNN
	1    3850 6400
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x02_Male J11
U 1 1 608DC25B
P 3850 5800
F 0 "J11" H 3822 5682 50  0000 R CNN
F 1 "READY2_LED" V 3800 6550 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B2B-XH-A_1x02_P2.50mm_Vertical" H 3850 5800 50  0001 C CNN
F 3 "~" H 3850 5800 50  0001 C CNN
	1    3850 5800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3750 5000 3850 5000
Wire Wire Line
	3700 6200 3850 6200
Wire Wire Line
	3750 5600 3850 5600
Text Label 3450 5000 2    50   ~ 0
D7_READY1_CLR_LED
Text Label 3450 5600 2    50   ~ 0
D6_READY2_CLR_LED
Text Label 3400 6200 2    50   ~ 0
D13_READY3_CLR_LED
$Comp
L power:GND #PWR014
U 1 1 605A2C56
P 3950 5600
F 0 "#PWR014" H 3950 5350 50  0001 C CNN
F 1 "GND" H 3955 5427 50  0000 C CNN
F 2 "" H 3950 5600 50  0001 C CNN
F 3 "" H 3950 5600 50  0001 C CNN
	1    3950 5600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR013
U 1 1 605A2736
P 3950 5000
F 0 "#PWR013" H 3950 4750 50  0001 C CNN
F 1 "GND" H 3955 4827 50  0000 C CNN
F 2 "" H 3950 5000 50  0001 C CNN
F 3 "" H 3950 5000 50  0001 C CNN
	1    3950 5000
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R10
U 1 1 605B4D0B
P 3550 6200
F 0 "R10" V 3343 6200 50  0000 C CNN
F 1 "330" V 3434 6200 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3480 6200 50  0001 C CNN
F 3 "~" H 3550 6200 50  0001 C CNN
	1    3550 6200
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 605B4591
P 3600 5600
F 0 "R12" V 3393 5600 50  0000 C CNN
F 1 "330" V 3484 5600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3530 5600 50  0001 C CNN
F 3 "~" H 3600 5600 50  0001 C CNN
	1    3600 5600
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 605AC4FE
P 3600 5000
F 0 "R11" V 3393 5000 50  0000 C CNN
F 1 "330" V 3484 5000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3530 5000 50  0001 C CNN
F 3 "~" H 3600 5000 50  0001 C CNN
	1    3600 5000
	0    1    1    0   
$EndComp
Text Label 1700 3350 0    50   ~ 0
D2_READY1_CLR_BTN
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 6084439A
P 850 3550
F 0 "J1" H 822 3432 50  0000 R CNN
F 1 "READY1_PB" V 650 3750 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B2B-XH-A_1x02_P2.50mm_Vertical" H 850 3550 50  0001 C CNN
F 3 "~" H 850 3550 50  0001 C CNN
	1    850  3550
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 6082CE95
P 850 4900
F 0 "J2" H 822 4782 50  0000 R CNN
F 1 "READY2_PB" V 650 5100 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B2B-XH-A_1x02_P2.50mm_Vertical" H 850 4900 50  0001 C CNN
F 3 "~" H 850 4900 50  0001 C CNN
	1    850  4900
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_01x02_Male J5
U 1 1 60807C77
P 2300 4100
F 0 "J5" H 2272 3982 50  0000 R CNN
F 1 "READY3_PB" V 2100 4300 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B2B-XH-A_1x02_P2.50mm_Vertical" H 2300 4100 50  0001 C CNN
F 3 "~" H 2300 4100 50  0001 C CNN
	1    2300 4100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	950  3350 1300 3350
Wire Wire Line
	700  3350 850  3350
Wire Wire Line
	700  4700 850  4700
Wire Wire Line
	2400 3900 2750 3900
Wire Wire Line
	2150 3900 2300 3900
Wire Wire Line
	950  4700 1300 4700
Text Label 3150 3900 0    50   ~ 0
D4_READY3_CLR_BTN
Text Label 1700 4700 0    50   ~ 0
D3_READY2_CLR_BTN
Connection ~ 3150 3900
Connection ~ 1700 4700
Wire Wire Line
	3150 3750 3150 3900
$Comp
L power:GND #PWR010
U 1 1 605C090C
P 3150 3450
F 0 "#PWR010" H 3150 3200 50  0001 C CNN
F 1 "GND" H 3155 3277 50  0000 C CNN
F 2 "" H 3150 3450 50  0001 C CNN
F 3 "" H 3150 3450 50  0001 C CNN
	1    3150 3450
	-1   0    0    1   
$EndComp
$Comp
L Device:R R7
U 1 1 605C0906
P 3150 3600
F 0 "R7" V 2943 3600 50  0000 C CNN
F 1 "10K" V 3034 3600 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3080 3600 50  0001 C CNN
F 3 "~" H 3150 3600 50  0001 C CNN
	1    3150 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	1700 4550 1700 4700
$Comp
L power:GND #PWR06
U 1 1 605BDF2D
P 1700 4250
F 0 "#PWR06" H 1700 4000 50  0001 C CNN
F 1 "GND" H 1705 4077 50  0000 C CNN
F 2 "" H 1700 4250 50  0001 C CNN
F 3 "" H 1700 4250 50  0001 C CNN
	1    1700 4250
	-1   0    0    1   
$EndComp
$Comp
L Device:R R4
U 1 1 605BDF27
P 1700 4400
F 0 "R4" V 1493 4400 50  0000 C CNN
F 1 "10K" V 1584 4400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1630 4400 50  0001 C CNN
F 3 "~" H 1700 4400 50  0001 C CNN
	1    1700 4400
	-1   0    0    1   
$EndComp
Connection ~ 1700 3350
Wire Wire Line
	1700 3200 1700 3350
$Comp
L power:GND #PWR04
U 1 1 605B96E4
P 1700 2900
F 0 "#PWR04" H 1700 2650 50  0001 C CNN
F 1 "GND" H 1705 2727 50  0000 C CNN
F 2 "" H 1700 2900 50  0001 C CNN
F 3 "" H 1700 2900 50  0001 C CNN
	1    1700 2900
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 605B900A
P 1700 3050
F 0 "R3" V 1493 3050 50  0000 C CNN
F 1 "10K" V 1584 3050 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1630 3050 50  0001 C CNN
F 3 "~" H 1700 3050 50  0001 C CNN
	1    1700 3050
	-1   0    0    1   
$EndComp
Wire Wire Line
	3150 4200 3150 4300
Wire Wire Line
	3050 3900 3150 3900
$Comp
L power:GND #PWR011
U 1 1 606050AC
P 3150 4300
F 0 "#PWR011" H 3150 4050 50  0001 C CNN
F 1 "GND" H 3155 4127 50  0000 C CNN
F 2 "" H 3150 4300 50  0001 C CNN
F 3 "" H 3150 4300 50  0001 C CNN
	1    3150 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 606050A6
P 3150 4050
F 0 "C5" H 3265 4096 50  0000 L CNN
F 1 "0.1uF" H 3265 4005 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3188 3900 50  0001 C CNN
F 3 "~" H 3150 4050 50  0001 C CNN
	1    3150 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 606050A0
P 2900 3900
F 0 "R6" V 2693 3900 50  0000 C CNN
F 1 "330" V 2784 3900 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 2830 3900 50  0001 C CNN
F 3 "~" H 2900 3900 50  0001 C CNN
	1    2900 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 5000 1700 5100
Wire Wire Line
	1600 4700 1700 4700
$Comp
L power:GND #PWR07
U 1 1 60603634
P 1700 5100
F 0 "#PWR07" H 1700 4850 50  0001 C CNN
F 1 "GND" H 1705 4927 50  0000 C CNN
F 2 "" H 1700 5100 50  0001 C CNN
F 3 "" H 1700 5100 50  0001 C CNN
	1    1700 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 6060362E
P 1700 4850
F 0 "C2" H 1815 4896 50  0000 L CNN
F 1 "0.1uF" H 1815 4805 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 1738 4700 50  0001 C CNN
F 3 "~" H 1700 4850 50  0001 C CNN
	1    1700 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 60603628
P 1450 4700
F 0 "R2" V 1243 4700 50  0000 C CNN
F 1 "330" V 1334 4700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1380 4700 50  0001 C CNN
F 3 "~" H 1450 4700 50  0001 C CNN
	1    1450 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 3650 1700 3750
Wire Wire Line
	1600 3350 1700 3350
$Comp
L power:GND #PWR05
U 1 1 605C01B8
P 1700 3750
F 0 "#PWR05" H 1700 3500 50  0001 C CNN
F 1 "GND" H 1705 3577 50  0000 C CNN
F 2 "" H 1700 3750 50  0001 C CNN
F 3 "" H 1700 3750 50  0001 C CNN
	1    1700 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 605BFCCC
P 1700 3500
F 0 "C1" H 1815 3546 50  0000 L CNN
F 1 "0.1uF" H 1815 3455 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 1738 3350 50  0001 C CNN
F 3 "~" H 1700 3500 50  0001 C CNN
	1    1700 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 605BF97B
P 1450 3350
F 0 "R1" V 1243 3350 50  0000 C CNN
F 1 "330" V 1334 3350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1380 3350 50  0001 C CNN
F 3 "~" H 1450 3350 50  0001 C CNN
	1    1450 3350
	0    1    1    0   
$EndComp
Connection ~ 6950 6100
Wire Wire Line
	4300 2300 4300 2450
$Comp
L Regulator_Linear:L7805 U5
U 1 1 608873B1
P 3550 2300
F 0 "U5" H 3550 2542 50  0000 C CNN
F 1 "L7805" H 3550 2451 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 3575 2150 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 3550 2250 50  0001 C CNN
	1    3550 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR033
U 1 1 6088F603
P 3550 2950
F 0 "#PWR033" H 3550 2700 50  0001 C CNN
F 1 "GND" H 3555 2777 50  0000 C CNN
F 2 "" H 3550 2950 50  0001 C CNN
F 3 "" H 3550 2950 50  0001 C CNN
	1    3550 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 2300 3200 2300
$Comp
L Device:C C8
U 1 1 608B1E41
P 3250 2800
F 0 "C8" H 3365 2846 50  0000 L CNN
F 1 "330nF" H 3365 2755 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3288 2650 50  0001 C CNN
F 3 "~" H 3250 2800 50  0001 C CNN
	1    3250 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 608B25E8
P 3850 2800
F 0 "C9" H 3965 2846 50  0000 L CNN
F 1 "0.1uF" H 3965 2755 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3888 2650 50  0001 C CNN
F 3 "~" H 3850 2800 50  0001 C CNN
	1    3850 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 2600 3550 2950
Connection ~ 3550 2950
Wire Wire Line
	3250 2950 3550 2950
Wire Wire Line
	3250 2650 3250 2300
Connection ~ 3250 2300
Wire Wire Line
	3550 2950 3850 2950
Wire Wire Line
	3850 2650 3850 2300
Connection ~ 3850 2300
Wire Wire Line
	3850 2300 3900 2300
Wire Wire Line
	3900 2150 3900 2300
Wire Wire Line
	4000 2300 3900 2300
Connection ~ 3900 2300
$Comp
L Mechanical:MountingHole_Pad H1
U 1 1 608C55FC
P 1200 1400
F 0 "H1" H 1050 1350 50  0000 C CNN
F 1 "4_18650_IN+" H 1200 1600 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1200 1400 50  0001 C CNN
F 3 "~" H 1200 1400 50  0001 C CNN
	1    1200 1400
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad H8
U 1 1 608C6317
P 1800 1400
F 0 "H8" H 1650 1350 50  0000 C CNN
F 1 "4_18650_IN-" H 1800 1600 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1800 1400 50  0001 C CNN
F 3 "~" H 1800 1400 50  0001 C CNN
	1    1800 1400
	1    0    0    -1  
$EndComp
Text Label 3150 2300 2    50   ~ 0
7.4v
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 608D9FF9
P 3200 2250
F 0 "#FLG0103" H 3200 2325 50  0001 C CNN
F 1 "PWR_FLAG" H 3200 2423 50  0000 C CNN
F 2 "" H 3200 2250 50  0001 C CNN
F 3 "~" H 3200 2250 50  0001 C CNN
	1    3200 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 2250 3200 2300
Connection ~ 3200 2300
Wire Wire Line
	3200 2300 3250 2300
Text Notes -2800 2100 0    50   ~ 0
This design uses Li-ion batteries, however using an sealed lead \nacid battery (SLA) is safer and should provide for more launches.
Text Notes -2800 2500 0    50   ~ 0
Two current limiting resistors have been added to the design to\nensure safety if the ignitor shorts and does not open during ignation.
Text Notes -2800 2950 0    50   ~ 0
A sloblow (slow blow) fuse has been added to the \nhigh voltage/high current 4-18650 current path to protect the\nbatteries and hardware if the relay was to weld in place.
Text Label 1200 1900 0    50   ~ 0
7.4v
$Comp
L Mechanical:MountingHole_Pad H9
U 1 1 608C76EA
P 1800 2000
F 0 "H9" H 1650 1950 50  0000 C CNN
F 1 "2_18650_IN-" H 1800 2200 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1800 2000 50  0001 C CNN
F 3 "~" H 1800 2000 50  0001 C CNN
	1    1800 2000
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad H2
U 1 1 608C6E53
P 1200 2000
F 0 "H2" H 1050 1950 50  0000 C CNN
F 1 "2_18650_IN+" H 1200 2200 50  0000 C CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965_Pad" H 1200 2000 50  0001 C CNN
F 3 "~" H 1200 2000 50  0001 C CNN
	1    1200 2000
	-1   0    0    1   
$EndComp
$Comp
L Device:C C11
U 1 1 60A4BD48
P 5500 800
F 0 "C11" H 5615 846 50  0000 L CNN
F 1 "0.1uF" H 5615 755 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 5538 650 50  0001 C CNN
F 3 "~" H 5500 800 50  0001 C CNN
	1    5500 800 
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C10
U 1 1 60A4CFE7
P 3900 800
F 0 "C10" H 4015 846 50  0000 L CNN
F 1 "0.1uF" H 4015 755 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3938 650 50  0001 C CNN
F 3 "~" H 3900 800 50  0001 C CNN
	1    3900 800 
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR035
U 1 1 60A4D506
P 5650 800
F 0 "#PWR035" H 5650 550 50  0001 C CNN
F 1 "GND" H 5655 627 50  0000 C CNN
F 2 "" H 5650 800 50  0001 C CNN
F 3 "" H 5650 800 50  0001 C CNN
	1    5650 800 
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR034
U 1 1 60A4DA0E
P 4050 800
F 0 "#PWR034" H 4050 550 50  0001 C CNN
F 1 "GND" H 4055 627 50  0000 C CNN
F 2 "" H 4050 800 50  0001 C CNN
F 3 "" H 4050 800 50  0001 C CNN
	1    4050 800 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3750 1000 3750 800 
Wire Wire Line
	5350 800  5350 1000
Wire Wire Line
	7250 2300 7650 2300
Wire Wire Line
	7800 6150 8200 6150
Text Notes 600  6350 0    50   ~ 0
Adding a current sensor or warning system\n to indicate that the high current (14.8v) relay \nhas not switched back to the non-launch \npostition would be a good safety additon \nto the design.
Wire Wire Line
	1800 1500 1800 1700
Text Label 1250 1600 0    50   ~ 0
14.8v
Wire Wire Line
	1200 1500 1200 1600
Wire Wire Line
	1200 1600 1250 1600
$Comp
L power:GND #PWR0101
U 1 1 60A94C9F
P 2000 1700
F 0 "#PWR0101" H 2000 1450 50  0001 C CNN
F 1 "GND" H 2005 1527 50  0000 C CNN
F 2 "" H 2000 1700 50  0001 C CNN
F 3 "" H 2000 1700 50  0001 C CNN
	1    2000 1700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2000 1700 1900 1700
Connection ~ 1800 1700
Wire Wire Line
	1800 1700 1800 1900
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 60A99585
P 2150 1500
F 0 "#FLG0101" H 2150 1575 50  0001 C CNN
F 1 "PWR_FLAG" H 2150 1673 50  0000 C CNN
F 2 "" H 2150 1500 50  0001 C CNN
F 3 "~" H 2150 1500 50  0001 C CNN
	1    2150 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 1500 1900 1500
Wire Wire Line
	1900 1500 1900 1700
Connection ~ 1900 1700
Wire Wire Line
	1900 1700 1800 1700
Wire Wire Line
	9450 3850 9450 4150
Wire Wire Line
	9700 3600 9750 3600
Wire Wire Line
	9450 3500 9750 3500
Wire Wire Line
	9300 3400 9750 3400
Wire Wire Line
	9700 3300 9750 3300
$Comp
L Connector:Conn_01x04_Male J20
U 1 1 60A2CC68
P 9950 3500
F 0 "J20" H 9922 3382 50  0000 R CNN
F 1 "LAUNCH_BTN_LED" H 10500 3200 50  0000 R CNN
F 2 "Connector_JST:JST_XH_B4B-XH-A_1x04_P2.50mm_Vertical" H 9950 3500 50  0001 C CNN
F 3 "~" H 9950 3500 50  0001 C CNN
	1    9950 3500
	-1   0    0    1   
$EndComp
Wire Wire Line
	9700 3600 9700 3850
Wire Wire Line
	9450 3550 9450 3500
$Comp
L Device:R R23
U 1 1 6064EDD6
P 9150 3400
F 0 "R23" V 8943 3400 50  0000 C CNN
F 1 "330" V 9034 3400 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9080 3400 50  0001 C CNN
F 3 "~" H 9150 3400 50  0001 C CNN
	1    9150 3400
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C6
U 1 1 6064EDDC
P 8900 3250
F 0 "C6" H 9015 3296 50  0000 L CNN
F 1 "0.1uF" H 9015 3205 50  0000 L CNN
F 2 "Capacitor_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 8938 3100 50  0001 C CNN
F 3 "~" H 8900 3250 50  0001 C CNN
	1    8900 3250
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR027
U 1 1 6064EDE2
P 8900 3000
F 0 "#PWR027" H 8900 2750 50  0001 C CNN
F 1 "GND" H 8905 2827 50  0000 C CNN
F 2 "" H 8900 3000 50  0001 C CNN
F 3 "" H 8900 3000 50  0001 C CNN
	1    8900 3000
	-1   0    0    1   
$EndComp
Wire Wire Line
	9000 3400 8900 3400
Wire Wire Line
	8900 3100 8900 3000
Text Label 8900 3400 2    50   ~ 0
A0_LAUNCH_BTN
$Comp
L Device:R R21
U 1 1 6064EDED
P 8900 3700
F 0 "R21" V 8693 3700 50  0000 C CNN
F 1 "10K" V 8784 3700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 8830 3700 50  0001 C CNN
F 3 "~" H 8900 3700 50  0001 C CNN
	1    8900 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR028
U 1 1 6064EDF3
P 8900 3850
F 0 "#PWR028" H 8900 3600 50  0001 C CNN
F 1 "GND" H 8905 3677 50  0000 C CNN
F 2 "" H 8900 3850 50  0001 C CNN
F 3 "" H 8900 3850 50  0001 C CNN
	1    8900 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 3550 8900 3400
Connection ~ 8900 3400
$Comp
L Device:R R26
U 1 1 6067ACBF
P 9450 3700
F 0 "R26" V 9243 3700 50  0000 C CNN
F 1 "330" V 9334 3700 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 9380 3700 50  0001 C CNN
F 3 "~" H 9450 3700 50  0001 C CNN
	1    9450 3700
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR032
U 1 1 6067B022
P 9700 3850
F 0 "#PWR032" H 9700 3600 50  0001 C CNN
F 1 "GND" H 9705 3677 50  0000 C CNN
F 2 "" H 9700 3850 50  0001 C CNN
F 3 "" H 9700 3850 50  0001 C CNN
	1    9700 3850
	1    0    0    -1  
$EndComp
Text Label 9450 4150 0    50   ~ 0
A2_LAUNCH_BTN_LED
Text Label 9700 3300 2    50   ~ 0
5v
Text Notes 650  5800 0    50   ~ 0
D13 is also the Nano \non-board LED and will \ngo HIGH when the \nchip is powered on.
Text Notes 550  7950 0    50   ~ 0
Creative Commons Corporation (“Creative Commons”) is not a\nlaw firm and does not provide legal services or legal advice. \nDistribution of Creative Commons public licenses does not create\na lawyer-client or other relationship. Creative Commons makes its\nlicenses and related information available on an “as-is” basis. \nCreative Commons gives no warranties regarding its licenses, any\nmaterial licensed under their terms and conditions, or any related\ninformation. Creative Commons disclaims all liability for damages\nresulting from their use to the fullest extent possible. By exercising\nthe Licensed Rights (defined below), You accept and agree to be\nbound by the terms and conditions of this Creative Commons\nAttribution-ShareAlike 4.0 International Public License ("Public\nLicense"). To the extent this Public License may be interpreted as \na contract, You are granted the Licensed Rights in consideration of\n Your acceptance of these terms and conditions, and the Licensor\ngrants You such rights in consideration of benefits the Licensor\nreceives from making the Licensed Material available under these\nterms and conditions.
$EndSCHEMATC
