//============================================================================
// model_rocket_launch_controller project nano source code
//
// This code is to be used with the Arduino Nano embedded in the 
// Model Rocket Launch controller project discussed in the video series
// starting with https://youtu.be/uxJpZ-KVTbc
//
// This work is licensed under the Creative Commons 
// Attribution-NonCommercial 4.0 International License. To view a copy
// of this license, visit http://creativecommons.org/licenses/by-nc/4.0/
//
// written by: Stuart Patterson 2021
//============================================================================
#include "Arduino.h"
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"    //https://wiki.dfrobot.com/DFPlayer_Mini_SKU_DFR0299

//#define DEBUG 1                     // 1 = debug display via serial monitor, comment out to turn off debugging

#define DFPLAYER_RX_PIN 10
#define DFPLAYER_TX_PIN 11

#define LAUNCH_ROCKET1_PIN 12
#define LAUNCH_ROCKET2_PIN 5

#define ROCKET1_SWITCH_PIN 8
#define ROCKET2_SWITCH_PIN 9

#define LAUNCH_BUTTON_PIN 14                //A0
#define LAUNCH_SAFETY_BUTTON_PIN 15         //A1
#define LAUNCH_BUTTON_LED_PIN 16            //A2
#define LAUNCH_SAFETY_BUTTON_LED_PIN 17     //A3
#define CONTINUITY_INPUT_ROCKET1_PIN 18     //A4 
#define CONTINUITY_VOLTAGE_OUT_PIN 19       //A5 
#define CONTINUITY_INPUT_ROCKET2_PIN 20     //A6 A6 and A7 can only be used as INPUTS
#define DFPLAYER_BUSY_PIN 21                //A7  0=playing, 1=not playing

#define MP3_READY1_CLEAR 1                  // the follow are the file names
#define MP3_READY2_CLEAR 2                  // to play for each of the states
#define MP3_READY3_CLEAR 3                  // the folder name needs to be mp3, placed under the SD card root directory, 
#define MP3_GO_FOR_LAUNCH 4                 // and the mp3 file name needs to be 4 digits, for example, "0001.mp3"
#define MP3_WE_HAVE_LAUNCH 5                // and the mp3 file name needs to be 4 digits, for example, "0001.mp3"
#define MP3_SCRUB_LAUNCH 6

#define READY1_CLEAR_BUTTON_PIN 2
#define READY1_CLEAR_LED_PIN 7

#define READY2_CLEAR_BUTTON_PIN 3
#define READY2_CLEAR_LED_PIN 6

#define READY3_CLEAR_BUTTON_PIN 4
#define READY3_CLEAR_LED_PIN 13

//#define IGNITION_TIME_MS 6000

SoftwareSerial DFSoftwareSerial(DFPLAYER_RX_PIN, DFPLAYER_TX_PIN); // RX, TX
DFRobotDFPlayerMini DFPlayer;

bool g_mp3_player;
bool g_ready1_clear;
bool g_ready2_clear;
bool g_ready3_clear;
bool g_go_for_launch;
bool g_sync_launch;
bool g_continuity_rocket1;
bool g_continuity_rocket2;
bool g_launch_rocket1;
bool g_launch_rocket2;
bool g_launch_safety_button_pressed;
bool g_launch_completed = false;
int g_launch_button_strobe = 0;
unsigned long g_launch_button_strobe_millis = 0;

//----------------------------------------------------------------------------
// powerOnLEDSequence()
//  cycles through the LEDs to indicate power has been applied and for fun!
//----------------------------------------------------------------------------
void powerOnLEDSequence()
{
  #ifdef DEBUG
    Serial.println("powerOnLEDSequence()");
  #endif

  for(int x=0; x < 3; ++x ) {
    digitalWrite(READY1_CLEAR_LED_PIN, HIGH);
    digitalWrite(READY2_CLEAR_LED_PIN, HIGH);
    digitalWrite(READY3_CLEAR_LED_PIN, HIGH);
    digitalWrite(LAUNCH_BUTTON_LED_PIN, HIGH);
    digitalWrite(LAUNCH_SAFETY_BUTTON_LED_PIN, HIGH);
  
    delay(500);    // one second delay
  
    digitalWrite(READY1_CLEAR_LED_PIN, LOW);
    digitalWrite(READY2_CLEAR_LED_PIN, LOW);
    digitalWrite(READY3_CLEAR_LED_PIN, LOW);
    digitalWrite(LAUNCH_BUTTON_LED_PIN, LOW);
    digitalWrite(LAUNCH_SAFETY_BUTTON_LED_PIN, LOW);

    delay(500);    // one second delay
  }
}


//----------------------------------------------------------------------------
//
//----------------------------------------------------------------------------
void setup() {

  #ifdef DEBUG
    Serial.begin(115200);
    Serial.println("setup()");
  #endif

  pinMode(READY1_CLEAR_BUTTON_PIN, INPUT);  // HIGH when selected
  pinMode(READY1_CLEAR_LED_PIN, OUTPUT);
  pinMode(READY2_CLEAR_BUTTON_PIN, INPUT);  // HIGH when selected
  pinMode(READY2_CLEAR_LED_PIN, OUTPUT);
  pinMode(READY3_CLEAR_BUTTON_PIN, INPUT);  // HIGH when selected
  pinMode(READY3_CLEAR_LED_PIN, OUTPUT);

  pinMode(ROCKET1_SWITCH_PIN, INPUT);  //
  pinMode(ROCKET2_SWITCH_PIN, INPUT);  //

  pinMode(CONTINUITY_INPUT_ROCKET1_PIN, INPUT);
  pinMode(CONTINUITY_INPUT_ROCKET2_PIN, INPUT);
  pinMode(CONTINUITY_VOLTAGE_OUT_PIN, OUTPUT);       // used to send 5v to the continutity test

  pinMode(DFPLAYER_RX_PIN, INPUT);
  pinMode(DFPLAYER_TX_PIN, OUTPUT);
  pinMode(DFPLAYER_BUSY_PIN, INPUT);

  pinMode(LAUNCH_ROCKET1_PIN, OUTPUT);
  pinMode(LAUNCH_ROCKET2_PIN, OUTPUT);

  pinMode(LAUNCH_BUTTON_PIN, INPUT);
  pinMode(LAUNCH_BUTTON_LED_PIN, OUTPUT);

  pinMode(LAUNCH_SAFETY_BUTTON_PIN, INPUT);
  pinMode(LAUNCH_SAFETY_BUTTON_LED_PIN, OUTPUT);

  powerOnLEDSequence();

  resetLaunchSystem();      // resets launch system for next launch
  DFSoftwareSerial.begin(9600);

  g_mp3_player = DFPlayer.begin(DFSoftwareSerial);

  // if the dfplayer is working, we will initalize its
  // starting configuration
  if ( g_mp3_player ) {
    DFPlayer.volume(30);  //Set volume value. From 0 to 30
    DFPlayer.EQ(DFPLAYER_EQ_NORMAL);
    DFPlayer.outputDevice(DFPLAYER_DEVICE_SD);
    DFPlayer.setTimeOut(500);
  }
}

//----------------------------------------------------------------------------
// resetLaunchSystem()
//  resets all of the global variables and processor pins to their 
//  default, prepare for launch, states
//----------------------------------------------------------------------------
void resetLaunchSystem()
{
  #ifdef DEBUG
    Serial.println("resetLaunchSystem()");
  #endif
  
  g_ready1_clear = false;
  g_ready2_clear = false;
  g_ready3_clear = false;
  g_go_for_launch = false;
  g_sync_launch = false;
  g_continuity_rocket1 = false;
  g_continuity_rocket2 = false;
  g_launch_rocket1 = false;
  g_launch_rocket2 = false;
  g_launch_button_strobe_millis = 0;

  digitalWrite(READY1_CLEAR_LED_PIN, LOW);
  digitalWrite(READY2_CLEAR_LED_PIN, LOW);
  digitalWrite(READY3_CLEAR_LED_PIN, LOW);

  digitalWrite(LAUNCH_BUTTON_LED_PIN, LOW);
  digitalWrite(LAUNCH_SAFETY_BUTTON_LED_PIN, LOW);
  digitalWrite(CONTINUITY_VOLTAGE_OUT_PIN, HIGH);
}

//----------------------------------------------------------------------------
// isDFPlayerBusy()
//  Called to determine if the dfplayer is already playing an mp3
//----------------------------------------------------------------------------
bool inline isDFPlayerBusy()
{
  #ifdef DEBUG
    Serial.println("isDFPlayerBusy()");
  #endif
  
  if ( g_mp3_player )
    return ( (analogRead(DFPLAYER_BUSY_PIN) > 500 ? false : true));
  else
    return (true);  // if dfplayer did not start, just return that it is busy!
}

//----------------------------------------------------------------------------
// DFPlayMP3(int)
//  called to play an mp3 if the player is not already busy. 
//  
//  below text taken from https://wiki.dfrobot.com/DFPlayer_Mini_SKU_DFR0299
//  The folder name needs to be mp3, placed under the SD card root directory, 
//  and the mp3 file name needs to be 4 digits, for example, "0001.mp3", 
//  placed under the mp3 folder. If you want to name it in Both English 
//  and Chinese, you can add it after the number, for example, "0001hello.mp3"
//----------------------------------------------------------------------------
void DFPlayMP3(int mp3)
{
  #ifdef DEBUG
    Serial.println("DFPlayMP3()");
  #endif
  
  if ( !isDFPlayerBusy() )
    DFPlayer.play(mp3);
}


//----------------------------------------------------------------------------
// loop()
//  this loop is called over and over again.  hence, state is maintained
//  in the global "g_" variables.
//----------------------------------------------------------------------------
void loop() {

  #ifdef DEBUG
    Serial.println("loop()");
  #endif

  //
  //scrub launch and reset happens if either rocket 1 or 2 has been selected for
  //launch via the switches, but then you decide cancel that rocket by 
  //switching off the toggle switch
  //
  if ( g_launch_rocket1 || g_launch_rocket2 ) {
    if ( digitalRead(ROCKET1_SWITCH_PIN) == LOW && digitalRead(ROCKET2_SWITCH_PIN) == LOW ) {
        if ( !g_launch_completed ) {
          DFPlayMP3(MP3_SCRUB_LAUNCH);
          resetLaunchSystem();
        }
        else {
          g_launch_completed = false;   // switched must be in the off position to start a new launch sequence
        }
    }
  }

  // which rocket do we want to launch
  // if both rocket 1 and rocket 2 are selected
  // that indicates a SYNC launch
  if ( digitalRead(ROCKET1_SWITCH_PIN) == HIGH )
    g_launch_rocket1 = true;
  else {
    g_launch_rocket1 = false;
  }

  if ( digitalRead(ROCKET2_SWITCH_PIN) == HIGH )
    g_launch_rocket2 = true;
  else
    g_launch_rocket2 = false;

  // if both rocket 1 and rocket 2 are selected
  // that indicates a SYNC launch
  if ( g_launch_rocket1 && g_launch_rocket2 )
    g_sync_launch = true;
  else
    g_sync_launch = false;


  // before we can run through the READY states
  // the user must first select which rocket they want 
  // to launch, once selected the user can now press
  // the ready states.  NO order is required.  Feel free
  // to change if you want!
  if ( g_launch_rocket1 || g_launch_rocket2 ) {
    
    if ( !g_ready1_clear ) {
      if ( digitalRead(READY1_CLEAR_BUTTON_PIN) == HIGH ) {
        DFPlayMP3(MP3_READY1_CLEAR);
        g_ready1_clear = true;
        digitalWrite(READY1_CLEAR_LED_PIN, HIGH);
      }
    }

    if ( !g_ready2_clear ) {
      if ( digitalRead(READY2_CLEAR_BUTTON_PIN) == HIGH ) {
        DFPlayMP3(MP3_READY2_CLEAR);
        g_ready2_clear = true;
        digitalWrite(READY2_CLEAR_LED_PIN, HIGH);
      }
    }

    if ( !g_ready3_clear ) {
      if ( digitalRead(READY3_CLEAR_BUTTON_PIN) == HIGH ) {
        DFPlayMP3(MP3_READY3_CLEAR);
        g_ready3_clear = true;
        digitalWrite(READY3_CLEAR_LED_PIN, HIGH);
      }
    }

    // now check for continuity at the rockets.
    // we check both, but only reference the one needed based on
    // the rocket that is selected to launch
    digitalWrite(CONTINUITY_VOLTAGE_OUT_PIN, HIGH);
    // analog read because of the voltage drop over the led and resistor
    #ifdef DEBUG
      Serial.print("analogRead(CONTINUITY_INPUT_ROCKET1_PIN) = ");
      Serial.println(analogRead(CONTINUITY_INPUT_ROCKET1_PIN));
      Serial.print("analogRead(CONTINUITY_INPUT_ROCKET2_PIN) = ");
      Serial.println(analogRead(CONTINUITY_INPUT_ROCKET2_PIN));      
    #endif    
    g_continuity_rocket1  = ( analogRead(CONTINUITY_INPUT_ROCKET1_PIN) < 100 ? true : false );
    g_continuity_rocket2  = ( analogRead(CONTINUITY_INPUT_ROCKET2_PIN) < 100 ? true : false );

    // all of our READY states are GO!
    if ( g_ready1_clear && g_ready2_clear && g_ready3_clear ) {

      if ( !g_go_for_launch ) {
        // are we GO for LAUNCH?
        if ( g_sync_launch ) {
          if ( g_continuity_rocket1 && g_continuity_rocket2 )
            g_go_for_launch = true;
        }
        else {
          if ( g_launch_rocket1 && g_continuity_rocket1 )
            g_go_for_launch = true;
          else if ( g_launch_rocket2 && g_continuity_rocket2 )
            g_go_for_launch = true;
        }
      }
    }
    else {
      g_go_for_launch = false;
    }

    // we are GO for LAUNCH!
    if ( g_go_for_launch ) {
  
      // turn on safety button led.  both safety and launch must be pressed to launch!
      digitalWrite(LAUNCH_SAFETY_BUTTON_LED_PIN, HIGH);
      
      if ( digitalRead(LAUNCH_SAFETY_BUTTON_PIN) == HIGH ) {
        
        if ( !g_launch_safety_button_pressed ) // only want to play it once
          DFPlayMP3(MP3_GO_FOR_LAUNCH);

        g_launch_safety_button_pressed = true;
  
        // strobe launch button
        if ( g_launch_button_strobe_millis == 0 || millis() - g_launch_button_strobe_millis > 500 ) {
          g_launch_button_strobe = (g_launch_button_strobe == 0 ? HIGH : LOW);
          digitalWrite(LAUNCH_BUTTON_LED_PIN, g_launch_button_strobe);
          g_launch_button_strobe_millis = millis();
        }
  
        // check that safety and launch button are both pressed to launch!
        if ( digitalRead(LAUNCH_SAFETY_BUTTON_PIN) == HIGH && digitalRead(LAUNCH_BUTTON_PIN) == HIGH  ) {

         // if the safety and launch are pressed we do a final continuity check and then launch!
          if ( g_sync_launch && g_continuity_rocket1 && g_continuity_rocket2 ) {
            DFPlayMP3(MP3_WE_HAVE_LAUNCH);
            digitalWrite(LAUNCH_ROCKET1_PIN, HIGH);
            digitalWrite(LAUNCH_ROCKET2_PIN, HIGH);
          }
          else if ( g_launch_rocket1 && g_continuity_rocket1 ) {
            DFPlayMP3(MP3_WE_HAVE_LAUNCH);
            digitalWrite(LAUNCH_ROCKET1_PIN, HIGH);
          }
          else if ( g_launch_rocket2 && g_continuity_rocket2 ) {
            DFPlayMP3(MP3_WE_HAVE_LAUNCH);
            digitalWrite(LAUNCH_ROCKET2_PIN, HIGH);
          }

          // experiments showed that 3 seconds was not reliabile to launch some igniters so it was determined to
          // allow the launch controller to determine timing by holding down the two buttons: safety and launch
          // if concerned, you could also addd a timing check to the loop, so they can hold it down for no longer than x seconds.
          while( digitalRead(LAUNCH_SAFETY_BUTTON_PIN) == HIGH && digitalRead(LAUNCH_BUTTON_PIN) == HIGH ) {   
          }

          if ( g_sync_launch && g_continuity_rocket1 && g_continuity_rocket2 ) {
            digitalWrite(LAUNCH_ROCKET1_PIN, LOW);
            digitalWrite(LAUNCH_ROCKET2_PIN, LOW);
          }
          else if ( g_launch_rocket1 && g_continuity_rocket1 ) {
            digitalWrite(LAUNCH_ROCKET1_PIN, LOW);
          }
          else if ( g_launch_rocket2 && g_continuity_rocket2 ) {
            digitalWrite(LAUNCH_ROCKET2_PIN, LOW);
          }
          
          resetLaunchSystem();
          g_launch_completed = true;
        }
      }  // end of safety and launch button check
      else if ( g_launch_safety_button_pressed && digitalRead(LAUNCH_SAFETY_BUTTON_PIN) == LOW ) {    // safety was released before launch!
        g_launch_safety_button_pressed = false;
        digitalWrite(LAUNCH_SAFETY_BUTTON_LED_PIN, LOW);
        digitalWrite(LAUNCH_BUTTON_LED_PIN, LOW);
      }
    }  // end of go for launch status
  } // end of rocket1 rocket2 selection check


  #ifdef DEBUG
    ouputDebugVariables();
  #endif
}

#ifdef DEBUG
void ouputDebugVariables()
{
  Serial.println("===<debug vars start>========");
  Serial.print("g_continuity_rocket1 = ");
  Serial.println(g_continuity_rocket1);

  Serial.print("g_continuity_rocket2 = ");
  Serial.println(g_continuity_rocket2);

  Serial.print("rocket 1 selected = ");
  Serial.println(g_launch_rocket1);

  Serial.print("rocket 2 selected = ");
  Serial.println(g_launch_rocket2);

  Serial.print("ready1 clear = ");
  Serial.println(g_ready1_clear);

  Serial.print("ready2 clear = ");
  Serial.println(g_ready2_clear);

  Serial.print("ready3 clear = ");
  Serial.println(g_ready3_clear);

  Serial.print("go for launch = ");
  Serial.println(g_go_for_launch);

  Serial.print("sync launch = ");
  Serial.println(g_sync_launch);
  Serial.println("===<debug vars end>========");
  Serial.println();
}
#endif
