# Model Rocket Launch Controller

[A 4 Part Video Series on Youtube](https://www.youtube.com/watch?v=uxJpZ-KVTbc&list=PLQi2cySxF1Tytb_yRExn4H6HWguOYwKfT)
for building this model rocket launch controller

Source for the Arduino Nano and KiCAD schematics, pcb, and gerber files available in the project directories. Enjoy!

This work is licensed under the Creative Commons 
Attribution-NonCommercial 4.0 International License. To view a copy
of this license, visit [http://creativecommons.org/licenses/by-nc/4.0/](http://creativecommons.org/licenses/by-nc/4.0/)


Any action you take upon the information in my YouTube videos or related schematics/stl/source code/additional content is strictly 
at your own risk and I will not be liable for losses, damages, or injuries in connection to the use of the videos or the recreation 
of the projects in the videos.  I am NOT a professional Electrical Engineer, nor am I licensed as an EE.